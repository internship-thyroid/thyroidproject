import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataestimatePage } from './dataestimate';

@NgModule({
  declarations: [
    DataestimatePage,
  ],
  imports: [
    IonicPageModule.forChild(DataestimatePage),
  ],
})
export class DataestimatePageModule {}
