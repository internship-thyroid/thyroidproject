import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
} from "ionic-angular";
import { GlobalProvider } from "../../../../providers/global/global";
import { Http, Headers, RequestOptions } from "@angular/http";
import moment from "moment";
import {
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import "moment/locale/TH";

/**
 * Generated class for the EditconditionultrasoundPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-editconditionultrasound',
  templateUrl: 'editconditionultrasound.html',
})
export class EditconditionultrasoundPage {
  date:string = moment().add(543, 'y').format("Do MMMM YYYY");
  nowdate:string = this.navParams.get("thy_che_date")
  presentDate: any;

  startMin: any;
  startMax: any;
  formgroup: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public global: GlobalProvider,
    public http: Http,
    public alertCtrl: AlertController) {
    this.startMin = moment().add(443, 'y').format("YYYY");
    this.startMax = moment().add(543, 'y').format("YYYY");
    this.presentDate = moment(this.nowdate,"Do MMM YYYY").format("YYYY-MM-DD");
    this.formgroup = formBuilder.group({
      ultradate: ["",],
      thy_che_size: [
        navParams.get("thy_che_size"),
        Validators.required,
      ],
      thy_che_found: [navParams.get("thy_che_found"), Validators.required],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditconditionultrasoundPage');
  }

  update() {
    let body = JSON.stringify({
      idcard: this.global.getpatientID(),
      round: this.global.getSelectRound(),

      thy_che_date: moment(this.formgroup.controls.ultradate.value).format("YYYY-MM-DD"),
      thy_che_size: this.formgroup.controls.thy_che_size.value,
      thy_che_found: this.formgroup.controls.thy_che_found.value,
    });
    console.log(body);

    let headers = new Headers({ "Content-type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    this.http
      .post(
        "https://" +
        this.global.getIP() +
        "/result.php?method=update_thy_che_date&role=" +
        this.global.getSelectRole(),
        body,
        options
      )
      .map((res) => res.json())
      .subscribe(
        (data) => {
          this.presentAlert(data.result);
          if (data.result !== "Fail") {
            this.navCtrl.pop();
          }
        },
        (error) => {
          console.log(error);
        }
      );
  }
  async presentAlert(txt: string) {
    let alert = await this.alertCtrl.create({
      title: "การแจ้งเตือน",
      subTitle: txt,
      buttons: ["Ok"],
    });
    alert.present();
  }
  async presentConfirm() {
    let alert = await this.alertCtrl.create({
      title: "ยืนยันการแก้ไขข้อมูล",
      message: "",
      buttons: [
        {
          text: "ยกเลิก",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "ยืนยัน",
          handler: () => {
            this.update();
          }
        }
      ]
    });
    alert.present();
  }

}
