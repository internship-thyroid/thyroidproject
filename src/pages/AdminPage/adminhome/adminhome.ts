import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events } from 'ionic-angular';
import {DatauserPage } from '../../AdminPage/datauser/datauser';
import {DatahosPage } from '../../AdminPage/datahos/datahos';
// import { DatacontactPage } from '../../AdminPage/datacontact/datacontact';
import { HomePage } from "../../Homepage/home/home";
import { ChangepassPage } from "../../changepass/changepass";

import { GlobalProvider } from "../../../providers/global/global";
import { Http, Headers, RequestOptions } from "@angular/http";
/**
 * Generated class for the AdminhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-adminhome',
  templateUrl: 'adminhome.html',
})
export class AdminhomePage {
  data: any = [];
  // csvData: any[] = [];

  constructor(public navCtrl: 
    NavController,
    public navParams: NavParams,
    public events: Events,
    private http: Http,
    public global: GlobalProvider,
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminhomePage');
  }

  user(){
    this.navCtrl.push(DatauserPage);
  }
  hos(){
    this.navCtrl.push(DatahosPage);
  }
  onClickLogoutButton(){
    this.events.publish("user:guest");
    this.navCtrl.setRoot(HomePage);
  }
  ChangePass() {
    this.navCtrl.push(ChangepassPage);
  }

  exportCSV() {
    let body = { search: this.navParams.get("search") };
    let headers = new Headers({ "Content-type": "application/json" });
    let options = new RequestOptions({ headers: headers });
    console.log(JSON.stringify(body));
    this.http
      .post(
        "https://" +
        this.global.getIP() +
        "/admin.php?method=patient_rpt&role=" +
        this.global.getSelectRole(),
        body,
        options
      )
      .map((res) => res.json())
      .subscribe(
        (data) => {
          this.data = data;
          console.log(data);

          let rows: object[];
          rows = data
          if (!rows || !rows.length) {
            return;
          }
          const separator = ',';
          const keys = Object.keys(rows[0]);
          const csvData =
            "\ufeff" + keys.join(separator) +
            '\n' +
            rows.map(row => {
              return keys.map(k => {
                let cell = row[k] === null || row[k] === undefined ? '' : row[k];
                cell = cell instanceof Date
                  ? cell.toLocaleString()
                  : cell.toString().replace(/"/g, '""');
                
                if (navigator.msSaveBlob) {
                    cell = cell.replace(/[^\x00-\x7F]/g, "");
                }

                if (cell.search(/("|,|\n)/g) >= 0) {
                  cell = `"${cell}"`;
                }
                return "\xa0"+cell;
              }).join(separator);
            }).join('\n');

          const blob = new Blob([csvData], { type: 'text/csv;encoding:utf-8' });
          var a = window.document.createElement("a");
          a.href = window.URL.createObjectURL(blob);
          a.download = "ExportData.csv";
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
        },
        (error) => {
          console.log(error);
        }
      );
  }

}
